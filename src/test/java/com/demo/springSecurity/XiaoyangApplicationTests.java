package com.demo.springSecurity;

import com.demo.springSecurity.sys.entity.SysUserRole;
import com.demo.springSecurity.sys.mapper.SysUserRoleMapper;
import com.demo.springSecurity.sys.mapper.UserMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;

@SpringBootTest
class XiaoyangApplicationTests {
    @Autowired
    private UserMapper userMapper;

    @Test
    void contextLoads() {
    }
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Test
    public void TestBCryptPasswordEncoder(){

//        System.out.println(passwordEncoder.matches("1234", "$2a$10$npv5JSeFR6/wLz8BBMmSBOMb8byg2eyfK4/vvoBk3RKtTLBhIhcpy"));
        String encode = passwordEncoder.encode("123");
        String encode2 = passwordEncoder.encode("123");
        System.out.println(encode);
        System.out.println(encode2);
    }


    @Autowired
    private SysUserRoleMapper sysUserRoleMapper;
    @Test
    public void insertid(){
        SysUserRole sysUserRole = new SysUserRole();
        sysUserRole.setUserId(4l);
        sysUserRole.setRoleId(444l);
        int insertid = sysUserRoleMapper.insert(sysUserRole);
        System.out.println(insertid);
    }

}
