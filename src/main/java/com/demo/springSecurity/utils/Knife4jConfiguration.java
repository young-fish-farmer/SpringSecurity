package com.demo.springSecurity.utils;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class Knife4jConfiguration {
    @Bean(value = "defaultApi2")
    /**
     * 根据自己的需求修改信息
     */
    public Docket defaultApi2() {
        Docket docket=new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(new ApiInfoBuilder()
                        .title("SpringSecurity")
                        .description("# swagger-bootstrap-ui-demo RESTful APIs")
                        .termsOfServiceUrl("http://www.xxx.com/")
                        .contact(new Contact("yyds","","123@qq.com"))
                        .version("1.8")
                        .build())
                //分组名称
                .groupName("2.X版本")
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.demo.springSecurity"))//这里指定Controller扫描包路径
                .paths(PathSelectors.any())
                .build();
        return docket;
    }
}