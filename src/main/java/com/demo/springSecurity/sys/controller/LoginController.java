package com.demo.springSecurity.sys.controller;


import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.demo.springSecurity.sys.service.LoginServcie;
import com.demo.springSecurity.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Api(tags = "登录模块")
@RestController
@RequestMapping("/user")
public class LoginController {

    @Autowired
    private LoginServcie loginServcie;

    /**
     * 用户登录
     * @param
     * @return
     */
    @ApiOperationSupport(author = "admin")
    @ApiOperation(value = "用户登录")
    @PostMapping("/login")
    public R login(@RequestParam String username,@RequestParam String password){
        //登录
        System.out.println("用户登录成功》》》》》》》");
        return loginServcie.login(username,password);
    }

    /**
     * 用户登出 同时删除Redis缓存的token
     * @return
     */
    @ApiOperationSupport(author = "admin")
    @ApiOperation(value = "用户登出注销token")
    @GetMapping("/logout")
    public R logout(){
        return loginServcie.logout();
    }
}
