package com.demo.springSecurity.sys.controller;

import com.demo.springSecurity.aop.LimitRequest;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.demo.springSecurity.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "权限认证测试")
@RestController
@RequestMapping("/oxx")
public class TestController {

    @ApiOperationSupport(author = "admin")
    @ApiOperation(value = "oaa权限接口才能访问")
    @LimitRequest(count = 3)
    @GetMapping("/hello")
    @PreAuthorize("@ex.hasAuthority('oaa')")
//    @PreAuthorize("hasAnyAuthority('admin','test','system:dept:list')")
//    @PreAuthorize("hasRole('oaa')")
//    @PreAuthorize("hasAnyRole('admin','system:dept:list')")
    public String hello(){
        return "访问成功";
    }

    @ApiOperationSupport(author = "admin")
    @ApiOperation(value = "oaa以下权限可以访问")
    @PreAuthorize("@ex.hasAuthority('obb')")
    @GetMapping("/testCors")
    public R testCors(){
        return  R.ok(200,"访问成功");
    }

    @ApiOperationSupport(author = "admin")
    @ApiOperation(value = "最低权限测试号")
    @PreAuthorize("@ex.hasAuthority('odd')")
    @GetMapping("/testCorss")
    public R testCorss(){
        return  R.ok(200,"访问成功");
    }
}
