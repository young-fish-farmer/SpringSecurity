package com.demo.springSecurity.sys.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.demo.springSecurity.sys.entity.SysUserRole;
import com.demo.springSecurity.sys.entity.User;
import com.demo.springSecurity.sys.mapper.MenuMapper;
import com.demo.springSecurity.sys.mapper.SysUserRoleMapper;
import com.demo.springSecurity.sys.mapper.UserMapper;
import com.demo.springSecurity.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@Api(tags = "注册模块")
@RestController
@RequestMapping("/reg")
public class RegController {
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private MenuMapper menuMapper;
    @Autowired
    private SysUserRoleMapper sysUserRoleMapper;

    @ApiOperationSupport(author = "admin")
    @ApiOperation(value = "用户注册")
    @PostMapping("/userReg")
    public R reg(@RequestParam String username,@RequestParam String password){
        if (username==null || password==null){
            return  R.ok(500,"用户名或者密码不能为空");
        }
        LambdaQueryWrapper<User> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(User::getUserName,username);
        User user = userMapper.selectOne(wrapper);
        if (user!=null){
            return  R.ok(500,"该用户已经存在");
        }
        String encode = passwordEncoder.encode(password);
        User u = new User();
        u.setUserName(username);
        u.setPassword(encode);
        u.setNickName("普通用户");
        u.setStatus("0");
        u.setDelFlag(0);
        u.setCreateTime(new Date());
        u.setUpdateTime(new Date());
        int insert = userMapper.insert(u);
        if (insert!=1){
            return R.ok(500,"注册失败");
        }
        User listName = userMapper.findName(username);
        if (listName!=null){
            Long userid =  listName.getId();
//            Long roleid = 444L;
            SysUserRole sur = new SysUserRole();
            sur.setUserId(userid);
            sur.setRoleId(444); //员工默认权限为普通员工
//            int insertid = sysUserRoleMapper.insertid(userid,roleid);
            int insert1 = sysUserRoleMapper.insert(sur);
            if (insert1==1){
                System.out.println("添加权限成功");
            }else {
                System.out.println("添加权限失败");
            }
        }
        return  R.ok(200,"添加用户权限成功");
    }
}
