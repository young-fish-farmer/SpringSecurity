package com.demo.springSecurity.sys.service;


import com.demo.springSecurity.utils.R;

public interface LoginServcie {
    /**
     * 用户登录
     * @param
     * @return
     */
    R login(String username, String password);

    /**
     * 用户登出 删除Redis与过滤器相关数据
     * @return
     */
    R logout();

}
