package com.demo.springSecurity.sys.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.demo.springSecurity.sys.entity.Menu;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface MenuMapper extends BaseMapper<Menu> {
    /**
     * 根据用户id查询用户权限数据
     * @param userid
     * @return
     */
    List<String> selectPermsByUserId(Long userid);


}
