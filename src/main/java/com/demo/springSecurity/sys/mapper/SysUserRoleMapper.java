package com.demo.springSecurity.sys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.demo.springSecurity.sys.entity.SysUserRole;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {

}
