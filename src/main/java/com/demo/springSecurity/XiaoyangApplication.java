package com.demo.springSecurity;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@Slf4j
public class XiaoyangApplication {

    public static void main(String[] args) {
        SpringApplication.run(XiaoyangApplication.class, args);
        log.info("项目启动成功");
        log.info("项目文档访问地址: http://localhost:8888/doc.html");
    }

}
